/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./dist/**/*.{html,js}"],
  theme: {
    container: {
      center: true,
      padding: '10rem',
    },
    colors: {
      'primary': '#82ae46',
      'danger': '#e4b2d6',
      'yellow': '#dcc698',
      'light': '#f7f6f2',
      'info': '#a2d1e1',
      'black': '#000000',
      'white': '#ffffff',
    },
    extend: {
    },
  },
  plugins: [],
}