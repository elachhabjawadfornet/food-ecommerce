



document.addEventListener('DOMContentLoaded', function () {


    let bannerSwiper = new Swiper('.banner-swiper', {
        loop: true,
        slidesPerView: 1,
        speed: 400,
    });


    let testimoniesSwiper = new Swiper('.testimonies-swiper', {

        loop: true,
        slidesPerView: 1,
        speed: 400,
        spaceBetween: 100,
        pagination: { el: '.swiper-pagination' },


    });

  
});
